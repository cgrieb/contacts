insert into contacts (id,first_name, last_name, phone_number, address, timezone, timezone_code, email, notes, owner) values(
    1,
    'Thomas',
    'Smith',
    '801-550-8970',
    '500 West 1220 South',
    'Mountain Standard Time',
    'MST',
    'thomas.smith@email.com',
    'This is the second test user',
    'role.admin@email.com'
);

insert into contacts (id,first_name, last_name, phone_number, address, timezone, timezone_code, email, notes, owner) values(
    2,
    'Bob',
    'Johnson',
    '801-571-2540',
    '1500 South Main',
    'Mountain Standard Time',
    'MST',
    'bob.johnson@email.com',
    'This is the first test user',
    'role.admin@email.com'
);


insert into contact_role (username,role) values('role.admin@email.com', 'admin');

insert into contact_user (username, password, first_name, last_name, enabled, role) values(
    'role.admin@email.com',
    'password',
    'Admin',
    'User',
    1,
    'admin'
);

insert into timezone (id,description,utc_code) values('ASK', 'Alaska Standard Time', 'GTM-09:00');
insert into timezone (id,description,utc_code) values('PST', 'Pacific Standard Time', 'GTM-08:00');
insert into timezone (id,description,utc_code) values('MST', 'Mountain Standard Time', 'GTM-07:00');
insert into timezone (id,description,utc_code) values('CST', 'Central Standard Time', 'GTM-06:00');
insert into timezone (id,description,utc_code) values('EST', 'Eastern Standard Time', 'GTM-05:00');
insert into timezone (id,description,utc_code) values('AST', 'Atlantic Standard Time', 'GTM-04:00');

insert into text (id,value) values('contacts.main.list.link', 'Get All Contacts');
insert into text (id,value) values('contacts.main.add.link', 'Add New Contact');
insert into text (id,value) values('contacts.main.main.link', 'Return to Main');
insert into text (id,value) values('contacts.main.nav.tab', 'Navigation');

