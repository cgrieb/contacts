package org.metropolis.contacts.configuration;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;
import java.util.List;

/**
 * Created by metropolis on 2/17/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = CacheConfig.class)
public class CacheConfigTest {
    private static final List<String> cacheNames = Arrays.asList("textCache", "contactCache",
            "timezoneCache");

    private static final int MAX_ENTRIES_LOCAL_HEAP = 1000;
    private static final int MAX_ENTRIES_LOCAL_DISK = 10000;
    private static final int TIME_T0_IDLE_AND_LIVE  = 45000;
    private static final int DISK_SPOOL_BUFFER_SIZE = 20;
    private static final String TRANSACTION_MODE = "OFF";
    private static final String MEMORY_EVICTION_POLICY = "LFU";

    @Test
    public void testCacheConfiguration() {
        CacheManager cacheManager = CacheManager.getInstance();
        cacheNames.forEach(cacheName ->{
            Cache cache = cacheManager.getCache(cacheName);
            net.sf.ehcache.config.CacheConfiguration cacheConfiguration = cache.getCacheConfiguration();
            Assert.assertEquals(cacheConfiguration.getMaxEntriesLocalHeap(), MAX_ENTRIES_LOCAL_HEAP);
            Assert.assertEquals(cacheConfiguration.getMaxEntriesLocalDisk(),MAX_ENTRIES_LOCAL_DISK);
            Assert.assertEquals(cacheConfiguration.getTimeToIdleSeconds(),TIME_T0_IDLE_AND_LIVE);
            Assert.assertEquals(cacheConfiguration.getTimeToLiveSeconds(),TIME_T0_IDLE_AND_LIVE);
            Assert.assertEquals(cacheConfiguration.getDiskSpoolBufferSizeMB(),DISK_SPOOL_BUFFER_SIZE);

            CacheConfiguration.TransactionalMode transactionalMode = cacheConfiguration.getTransactionalMode();
            Assert.assertEquals(transactionalMode.name(), TRANSACTION_MODE);

            MemoryStoreEvictionPolicy memoryStoreEvictionPolicy = cacheConfiguration.getMemoryStoreEvictionPolicy();
            Assert.assertEquals(memoryStoreEvictionPolicy.toString(), MEMORY_EVICTION_POLICY);
        });
    }
}
