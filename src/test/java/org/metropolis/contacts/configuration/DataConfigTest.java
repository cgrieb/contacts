package org.metropolis.contacts.configuration;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Created by metropolis on 2/17/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = DataConfig.class)
public class DataConfigTest {
    @Autowired
    DataSource dataSource;

    private static final Logger LOGGER = Logger.getLogger(DataConfigTest.class.getName());

    //*******************************************************************
    // Note: you'll need to update these finals to your specific database
    // information other wise it'll fail.
    //*******************************************************************
    private static final String URL = "jdbc:mysql://localhost:3306/metropolis";
    private static final String USERNAME = "root@localhost";

    @Test
    public void verifyDatabaseConnection() {
        try {
            Connection connection = dataSource.getConnection();
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            Assert.assertEquals(databaseMetaData.getUserName(), USERNAME);
            Assert.assertEquals(databaseMetaData.getURL(), URL);
        } catch (SQLException e) {
            LOGGER.warning(e.getMessage());
        }
    }
}
