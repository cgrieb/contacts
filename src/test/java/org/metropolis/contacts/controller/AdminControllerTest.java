package org.metropolis.contacts.controller;

import org.junit.Before;
import org.junit.Test;
import org.metropolis.contacts.ContactsContextBootStrap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by metropolis on 2/16/2017.
 */
public class AdminControllerTest extends ContactsContextBootStrap {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static final String ADMIN_USER = "role.admin@email.com";
    private static final String ADMIN_PASSWORD = "password";
    private static final String ADMIN_ROLE = "admin";

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testClearCache() throws Exception {
        mockMvc.perform( get("/admin/clearCache").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());
    }

}

