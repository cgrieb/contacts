package org.metropolis.contacts.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.metropolis.contacts.ContactsContextBootStrap;
import org.metropolis.contacts.entity.Contact;
import org.metropolis.contacts.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import java.nio.charset.Charset;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by metropolis on 2/13/2017.
 */
public class ContactControllerTest extends ContactsContextBootStrap {
    private MockMvc mockMvc;

    private static final String ADMIN_USER = "role.admin@email.com";
    private static final String ADMIN_PASSWORD = "password";
    private static final String ADMIN_ROLE = "admin";

    private static ObjectMapper objectMapper;

    private static Contact contact;

    @Resource
    private ContactRepository contactRepository;

    private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        contact = new Contact();

        contact.setId(3);
        contact.setFirstName("New");
        contact.setLastName("Contact");
        contact.setOwner(ADMIN_USER);
        contact.setAddress("500 West Temple");
        contact.setPhoneNumber("801-897-365");
        contact.setTimeZone("Mountain Standard Time");
        contact.setTimezoneCode("MST");
        contact.setNotes("This is a new user");
        contact.setEmail("new.user@email.com");

        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testAddNewContact() throws Exception {
        Contact contact = new Contact();

        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String request = objectWriter.writeValueAsString(contact);

        mockMvc.perform(post("/contacts/addNewContact").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)).contentType(APPLICATION_JSON_UTF8).
                content(request)).andExpect(status().isOk());
    }

    @Test
    public void testSaveContact() throws Exception {
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String request = objectWriter.writeValueAsString(contact);

        mockMvc.perform(post("/contacts/saveContact").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)).contentType(APPLICATION_JSON_UTF8).
                content(request)).andExpect(status().isOk());
    }

    @Test
     public void testDeleteContact() throws Exception {
        Contact contact = contactRepository.findOne(1);
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String request = objectWriter.writeValueAsString(contact);
        mockMvc.perform( post("/contacts/deleteContact").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)).contentType(APPLICATION_JSON_UTF8).
                content(request)).andExpect(status().isOk());

    }

    @Test
    public void testGetContacts() throws Exception {
        mockMvc.perform( get("/getContacts").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

    }

    @Test
    public void testGetFields() throws Exception {
        mockMvc.perform( get("/contacts/getFields").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

    }

    @Test
    public void testGetContactsSearch() throws Exception {
        mockMvc.perform( get("/contacts/getContacts/Johnson").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

    }

    @Test
    public void testSearchContacts() throws Exception {
        mockMvc.perform( get("/contacts/searchContacts").param("searchValue", "bob.johnson@email.com").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

    }

    @Test
    public void getGetTimeZones() throws Exception {
        mockMvc.perform( get("/contacts/getTimezones").param("searchValue", "bob.johnson@email.com").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

    }
 }
