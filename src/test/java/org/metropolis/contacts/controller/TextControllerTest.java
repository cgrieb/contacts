package org.metropolis.contacts.controller;

import org.junit.Before;
import org.junit.Test;
import org.metropolis.contacts.ContactsContextBootStrap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by metropolis on 2/16/2017.
 */
public class TextControllerTest extends ContactsContextBootStrap {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup (){
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testAuthStatus() throws Exception {
        mockMvc.perform(get("/text/getText")).andExpect(status().isOk());
    }
}
