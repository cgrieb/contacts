package org.metropolis.contacts.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.metropolis.contacts.ContactsContextBootStrap;
import org.metropolis.contacts.entity.User;
import org.metropolis.contacts.helper.AuthHeaderBuilder;
import org.metropolis.contacts.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import java.nio.charset.Charset;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by metropolis on 2/16/2017.
 */
public class AuthControllerTest extends ContactsContextBootStrap {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static final String ADMIN_USER = "role.admin@email.com";
    private static final String ADMIN_PASSWORD = "password";
    private static final String ADMIN_ROLE = "admin";

    @Resource
    private AuthHeaderBuilder authHeaderBuilder;

    @Resource
    private UserRepository userRepository;

    private static ObjectMapper objectMapper;

    private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));


    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testGetUserDetails() throws Exception {
        mockMvc.perform(get("/auth/getUserDetails").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());
    }

    @Test
    public void testAuthStatus() throws Exception {
        mockMvc.perform(post("/auth/getAuthStatus").param("userName", "role.admin@email.com").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());
    }

    @Test
    public void login() throws Exception {
        String credentials = authHeaderBuilder.generateAuthHeader(ADMIN_USER, ADMIN_PASSWORD);
        mockMvc.perform(get("/auth/login").header("Authentication", credentials))
                .andExpect(status().isOk());
    }

    @Test
    public void logout() throws Exception {
        mockMvc.perform(get("/auth/logout").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateUserAccount() throws Exception {
        User user = new User();
        user.setEnabled(true);
        user.setFirstName("New");
        user.setLastName("User");
        user.setUsername("new.user@email.com");
        user.setRole("admin");
        user.setPassword("password");

        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String request = objectWriter.writeValueAsString(user);

        mockMvc.perform(post("/auth/createUserAccount").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)).contentType(APPLICATION_JSON_UTF8).content(request))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateUserDetails() throws Exception {
        User user = userRepository.findOne("role.admin@email.com");

        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String request = objectWriter.writeValueAsString(user);

        mockMvc.perform(post("/auth/updateUserDetails").with(user(ADMIN_USER).password(ADMIN_PASSWORD).roles(ADMIN_ROLE)).contentType(APPLICATION_JSON_UTF8).content(request))
                .andExpect(status().isOk());
    }
}
