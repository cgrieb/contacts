package org.metropolis.contacts.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.metropolis.contacts.ContactsContextBootStrap;
import org.metropolis.contacts.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by metropolis on 2/16/2017.
 */
public class ContactsServiceTest extends ContactsContextBootStrap {
    @Resource
    private ContactService contactService;

    private  UserDetails userDetails;

    @Resource
    private AuthenticationManager authenticationManager;

    private static final String ADMIN_USER = "role.admin@email.com";
    private static final String ADMIN_PASSWORD = "password";

    @Before
    public void setup() {
        userDetails = this.setAuthentication();
    }

    @Test
    public void testGetAllContacts() {
        List<Contact> contactList = contactService.getAllContacts(userDetails);
        Assert.assertEquals(contactList.size(), 1);
    }

    @Test
    public void testFindContact() {
        List<Contact> contactList = contactService.findContacts("bob.johnson@email.com");
        Assert.assertEquals(contactList.size(),1);
    }

    @Test
    public void testGetText() {
        List<Text> textList = contactService.getText();
        Assert.assertEquals(textList.size(),4);
    }

    @Test
    public void testGetTimezones() {
        List<Timezone> timezoneList = contactService.getTimezones();
        Assert.assertEquals(timezoneList.size(), 6);
    }

    @Test
    public void testAddAndDeleteNewContact() {
        Contact contact = new Contact();
        contact.setFirstName("New");
        contact.setLastName("Contact");
        contact.setEmail("new.contact@email.com");
        contact.setId(3);
        contact.setNotes("This is a new contact");
        contact.setPhoneNumber("801-647-1111");
        contact.setTimeZone("'Mountain Standard Time");
        contact.setTimezoneCode("MST");
        contact.setOwner("role.admin@email.com");
        contact.setAddress("9781 West Temple");

        contactService.saveExistingContact(contact, userDetails);

        List<Contact> contactList = contactService.findContacts("new.contact@email.com");
        Assert.assertEquals(contactList.size(),1);

        contactService.deleteExistingContact(contactList.get(0), userDetails);
    }

    @Test
    public void testSaveExistingContact() {
        List<Contact> contactList = contactService.findContacts("bob.johnson@email.com");
        Assert.assertEquals(contactList.size(),1);
        Contact contact = contactList.get(0);
        contactService.saveExistingContact(contact,userDetails);
    }


    public UserDetails setAuthentication() {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(ADMIN_USER, ADMIN_PASSWORD);
        Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDetails;
    }
}
