package org.metropolis.contacts.service;

import org.junit.Assert;
import org.junit.Test;
import org.metropolis.contacts.ContactsContextBootStrap;
import org.metropolis.contacts.entity.Text;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by metropolis on 2/16/2017.
 */
public class TextServiceTest extends ContactsContextBootStrap {
    @Resource
    private TextService textService;

    @Test
    public void verifyGetAllText() {
        List<Text> textList = textService.getAllText();
        Assert.assertEquals(textList.size(), 4);
    }
}
