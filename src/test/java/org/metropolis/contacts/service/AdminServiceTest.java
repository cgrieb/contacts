package org.metropolis.contacts.service;

import org.junit.Assert;
import org.junit.Test;
import org.metropolis.contacts.ContactsContextBootStrap;

import javax.annotation.Resource;

/**
 * Created by metropolis on 2/16/2017.
 */
public class AdminServiceTest extends ContactsContextBootStrap {
    @Resource
    private AdminService adminService;

    @Test
    public void verifyClearCache() {
        String results = adminService.clearCache();
        Assert.assertNotNull(results);
    }
}
