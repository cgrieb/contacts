package org.metropolis.contacts.service;

import org.junit.Assert;
import org.junit.Test;
import org.metropolis.contacts.ContactsContextBootStrap;

import javax.annotation.Resource;

/**
 * Created by metropolis on 2/16/2017.
 */
public class PasswordEncoderTest extends ContactsContextBootStrap {
    @Resource
    private PasswordEncoder passwordEncoder;
    private static final String encodedPassword = "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8";

    @Test
    public void verifyEncodeUserPassword() {
        String password = passwordEncoder.encodeUserPassword("password");
        Assert.assertEquals(password,encodedPassword);
    }
}
