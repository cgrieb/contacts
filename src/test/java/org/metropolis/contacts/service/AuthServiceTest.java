package org.metropolis.contacts.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.metropolis.contacts.ContactsContextBootStrap;
import org.metropolis.contacts.entity.User;
import org.metropolis.contacts.helper.AuthHeaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import javax.annotation.Resource;

/**
 * Created by metropolis on 2/17/2017.
 */
public class AuthServiceTest extends ContactsContextBootStrap {
    @Autowired
    private AuthService authService;

    @Resource
    private  AuthenticationManager authenticationManager;

    @Resource
    private AuthHeaderBuilder authHeaderBuilder;

    private  UserDetails userDetails;

    private static final String ADMIN_USER = "role.admin@email.com";
    private static final String ADMIN_PASSWORD = "password";

    @Before
    public void setup() {
        userDetails = this.setAuthentication();
    }

    @Test
    public void testGetUserDetails() {
        User user = authService.getUserDetails();
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getUsername(),"role.admin@email.com");
    }

    @Test
    public void testAuthStatus() {
        boolean authenticated = authService.getAuthStatus("role.admin@email.com");
        Assert.assertTrue(authenticated);
    }

    @Test
    public void verifyLoginAndLogout() {
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
        mockHttpServletRequest.addHeader("Authentication", authHeaderBuilder.generateAuthHeader(ADMIN_USER,ADMIN_PASSWORD));
        boolean results = authService.login(mockHttpServletRequest);
        Assert.assertTrue(results);
        results = authService.logout();
        Assert.assertTrue(results);
        userDetails = this.setAuthentication();
    }

    @Test
    public void testAddUser() {
        User user = new User();
        user.setUsername("another.new.user@email.com");
        user.setFirstName("Another");
        user.setLastName("User");
        user.setPassword("password");
        user.setRole("admin");
        user.setEnabled(true);

        boolean results = authService.addUser(user);
        Assert.assertTrue(results);
    }

    @Test
    public void testGetCurrentUsername() {
        String currentName = authService.getCurrentUsername();
        Assert.assertEquals(currentName, ADMIN_USER);
    }

    @Test
    public void verifyUpdateUserProfile() {
        User user = authService.getUserDetails();
        authService.updateUserProfile(user,userDetails);
    }

    private UserDetails setAuthentication() {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(ADMIN_USER, ADMIN_PASSWORD);
        Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDetails;
    }
}