package org.metropolis.contacts;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.metropolis.contacts.configuration.CacheConfigTest;
import org.metropolis.contacts.configuration.DataConfigTest;
import org.metropolis.contacts.controller.AdminControllerTest;
import org.metropolis.contacts.controller.AuthControllerTest;
import org.metropolis.contacts.controller.ContactControllerTest;
import org.metropolis.contacts.controller.TextControllerTest;
import org.metropolis.contacts.service.*;

/**
 * Created by metropolis on 2/17/2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        AuthControllerTest.class,
        ContactControllerTest.class,
        TextControllerTest.class,
        AdminControllerTest.class,
        AdminServiceTest.class,
        AuthServiceTest.class,
        ContactsServiceTest.class,
        PasswordEncoderTest.class,
        TextServiceTest.class,
        DataConfigTest.class,
        CacheConfigTest.class
})
public class RunAllTests extends ContactsContextBootStrap  {
}
