package org.metropolis.contacts.helper;

import org.springframework.stereotype.Service;

import java.util.Base64;

/**
 * Created by metropolis on 2/16/2017.
 */
@Service
public class AuthHeaderBuilder {
    private static Base64.Encoder encoder = Base64.getEncoder();
    public String generateAuthHeader(String username, String password) {
        String authHeaderString = username + ":" + password;
        return "Basic " + encoder.encodeToString(authHeaderString.getBytes());
    }
}
