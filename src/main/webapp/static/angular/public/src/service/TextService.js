/**
 * Created by cgrieb on 2/14/16.
 */
angular.module('ContactsApp').service('TextService', ['$http','$q', function($http,$q) {
    var timeout = 4000;
    this.getText = function() {
        var deferred = $q.defer();
        $http({
            method:'GET',
            url:'/text/getText',
            timeout:timeout
        }).success(function(response){
           deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.getStoredValue = function(id) {
        if(!!Storage) {
            return sessionStorage.getItem(id);
        }
    };

    this.storeTextValue = function(id,value) {
        if(!!Storage) {
            var storedValue = sessionStorage.getItem(id);
            // If the text value is not cached or has changed, insert it.
            if (storedValue === undefined || storedValue === null||storedValue !== value) {
                sessionStorage.setItem(id,value);
            }
        }
    };
}]);