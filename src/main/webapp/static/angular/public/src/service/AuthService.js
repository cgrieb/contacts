/**
 * Created by cgrieb on 9/24/16.
 */
angular.module('ContactsApp').service('AuthService', ['$http','$q','$window', function($http,$q,$window) {
    var timeout = 40000;
    var userDetails = {};
    var userDetailsKey = "userDetails";

    this.getUserDetails = function() {
        var deferred = $q.defer();
        if(Object.keys(userDetails).length !== 0) {
            deferred.resolve(userDetails);
        } else {
            $http({
                method: "GET",
                url: "/auth/getUserDetails",
                timeout: timeout
            }).success(function(response){
                deferred.resolve(response);
            });
        }
        return deferred.promise;
    };

    this.login = function(authHeader) {
        var deferred = $q.defer();
        $http({
            method: "GET",
            url: "/auth/login",
            timeout: timeout,
            headers: {'Authentication':authHeader}
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.logout = function() {
        var deferred = $q.defer();
        $http({
            method: "GET",
            url: "/auth/logout",
            timeout: timeout
        }).success(function(){
            deferred.resolve(true);
        }).error(function() {
            deferred.resolve(false);
        });

        return deferred.promise;
    };

    this.createAccount = function(user) {
        var deferred = $q.defer();
        $http({
            method: "POST",
            url: "/auth/createUserAccount",
            data:JSON.stringify(user),
            timeout: timeout
        }).success(function(response){
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    this.postLogoutOperation = function(logoutStatus) {
        if(logoutStatus) {
            console.log("** user successfully logged out **");
            $window.location = "/login";
        } else {
            console.log("** there was an error logging out **")
        }
    };

    this.updateUserDetails = function(userDetails) {
        var deferred = $q.defer();
        $http({
            url:'/auth/updateUserDetails',
            method:'POST',
            timeout:timeout,
            data:JSON.stringify(userDetails)
        }).success(function(){
            deferred.resolve(true)
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.existingUserDetails = function(){
        return JSON.parse(sessionStorage.getItem(userDetailsKey));
    };

    this.setExistingUserDetails = function(userDetails) {
        sessionStorage.removeItem(userDetails);
        sessionStorage.setItem(userDetailsKey,JSON.stringify(userDetails));
    }
}]);