/**
 * Created by cgrieb on 1/17/16.
 */
angular.module('ContactsApp').service('ContactService',['$http','$q', function ($http,$q) {
    this.contact = {};
    var timeout = 40000;

    this.getContacts = function() {
        var deferred = $q.defer();
        $http({
            method: "GET",
            url: "/contacts/getContacts",
            timeout: timeout
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.saveContact = function(data) {
        var deferred = $q.defer();
        $http({
            method: "POST",
            url: "/contacts/saveContact",
            timeout: timeout,
            data:JSON.stringify(data)
        }).success(function(){
            deferred.resolve(true);
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.deleteContact = function(id) {
        var deferred = $q.defer();
        $http({
            method: "POST",
            url: "/contacts/deleteContact/" + id,
            timeout: timeout
        }).success(function(response){
             deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.addNewContact = function(contact) {
        var deferred = $q.defer();
        $http({
            method:"POST",
            url: "/contacts/addNewContact",
            timeout:timeout,
            data:JSON.stringify(contact)
        }).success(function(response){
            deferred.resolve(true)
        }).error(function(response){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.addContact = function(data) {
        var deferred = $q.defer();
        $http({
            method:"POST",
            url: "/contacts/addContact",
            timeout:timeout,
            params:data
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise
    };

    this.searchContacts = function(searchValue) {
        var deferred = $q.defer();
        $http({
            method:"GET",
            url: "/contacts/searchContacts",
            timeout:timeout,
            params:{searchValue:searchValue}
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.getTimezone = function() {
        var deferred = $q.defer();
        $http({
            method: "GET",
            url: "/contacts/getTimezones",
            timeout:timeout
        }).success(function(response){
           deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.setContacts = function(contact) {
        this.contact = contact;
    };

    this.returnContacts = function() {
        return this.contact;
    };
}]);
