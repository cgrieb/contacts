/**
 * Created by cgrieb on 2/14/16.
 */
angular.module('ContactsApp').filter('text',['TextService', function(TextService){
   function getText(id) {
       if (id) {
           var value = TextService.getStoredValue(id);
           return value;
       }
   }
    getText.$stateful = true;
   return getText;
}]);
