/**
 * Created by cgrieb on 2/16/16.
 */
angular.module('ContactsApp').controller("RunController", ['$scope','TextService','AuthService','$location','$interval','$document','$window', function($scope, TextService, AuthService,$location, $interval,$document,$window) {
    $scope.userDetails = {};
    var ANONYMOUS_USER = "anonymousUser";
    $scope.authenticated = false;

    var interval = 10;
    var max = 180;

    var url = $location.path();

    $interval(intervalTracker,5000);

    TextService.getText().then(function(response) {
        angular.forEach(response, function (value, key) {
            TextService.storeTextValue(value.id, value.value);
        });
    });

    AuthService.getUserDetails().then(function(response){
        $scope.userDetails = response;
        AuthService.setExistingUserDetails($scope.userDetails);
        if($scope.userDetails.username === ANONYMOUS_USER) {
            $location.path('/login').replace();
        } else {
            $scope.authenticated = true;
            $location.path(url).replace();
        }
    });

    function intervalTracker() {
        if($scope.authenticated) {
            if(interval>max) {
                console.log("** session has timed out - logging out user now **");
                logout();
                interval = 0;
            } else {
                interval++;
            }
        }
    }

    $document.find('body').on('mousemove mouseup keyup', function(){
        interval = 0;
    });

    var logout = function() {
        AuthService.logout().then(function(response){
            AuthService.postLogoutOperation(response);
        });
    }
}]);