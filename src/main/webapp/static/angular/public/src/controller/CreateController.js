/**
 * Created by cgrieb on 10/2/16.
 */
angular.module('ContactsApp').controller("CreateController", ['$scope','$window','$interval','AuthService', function($scope,$window,$interval, AuthService) {
    $scope.username = "";
    $scope.password = "";
    $scope.firstName = "";
    $scope.lastName = "";

    $scope.submitUserAccount = function() {
        var user = {
            username:$scope.username,
            password:$scope.password,
            firstName:$scope.firstName,
            lastName:$scope.lastName,
            enabled:true,
            role:'user'
        };

        AuthService.createAccount(user).then(function(response) {
            if(response) {
                console.log("** account created successfully for user " + $scope.username + " **");
                $window.location = "/login";
            } else {
                $scope.resetCreateAccountValues();
            }
        })
    };

    $scope.resetCreateAccountValues = function() {
        $scope.username = "";
        $scope.password = "";
        $scope.firstName = "";
        $scope.lastName = "";
    }
}]);