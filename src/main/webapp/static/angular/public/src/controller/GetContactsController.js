/**
 * Created by cgrieb on 1/17/16.
 */
angular.module('ContactsApp').controller('GetContactsController',['$scope', 'ContactService','AuthService','$location','$route','$window', function($scope, ContactService,AuthService,$location,$route,$window) {
    $scope.contacts = [];
    $scope.fields = [];
    $scope.noContacts = false;
    $scope.noSearchResults = false;
    $scope.searchValue = "";
    $scope.searchValueDisplay = "";
    $scope.inSearch = false;
    $scope.userDetails = {};

    ContactService.getContacts().then(function(response) {
        if ($scope.noContacts === true) {
            $scope.noContacts = false;

        }
        $scope.contacts = response;

        if ($scope.contacts.length == 0) {
            $scope.noContacts = true;
        }

    });

    $scope.isContactsEmpty = function() {
        for (var data in $scope.contacts) if ($scope.contacts.hasOwnProperty(data)) return true;
        return false;
    };

    $scope.navigateToEdit = function(contact) {
        ContactService.setContacts(contact);
        $location.path("/edit");
    };

    $scope.deleteContact = function(id) {
        ContactService.deleteContact(id).then(function(response){
            if (parseInt(response) !== 1) {
                console.warn("** Contact delete results are not 1 - possible hibernate update related issue **");
            }
        });
        window.location.reload();
    };

    $scope.runSearch = function() {
        $scope.inSearch = true;
        ContactService.searchContacts($scope.searchValue).then(function(response){
            $scope.contacts = response;
            if($scope.contacts.length === 0) {
                $scope.noSearchResults = true;
                $scope.searchValueDisplay = $scope.searchValue;
            }
            $scope.searchValue = "";
        });
    };

    $scope.resetSearch = function() {
        $scope.inSearch = false;
        $route.reload();
    };

    $scope.getSearchValueStatus = function() {
        return $scope.searchValue.length;
    };
}]);