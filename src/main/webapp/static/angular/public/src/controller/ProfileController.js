/**
 * Created by cgrieb on 10/14/16.
 */
angular.module('ContactsApp').controller("ProfileController", ['$scope','$window','AuthService', function($scope,$window, AuthService) {
    $scope.userDetails = AuthService.existingUserDetails();
    $scope.userDetailsOriginal = {};
    angular.copy($scope.userDetails,$scope.userDetailsOriginal);

    $scope.resetUserDetails = function() {
        angular.copy($scope.userDetailsOriginal,$scope.userDetails);
    };

    $scope.updateUserDetails = function() {
        AuthService.updateUserDetails($scope.userDetails).then(function(response){
            if(response) {
                console.log("** user profile update **");
                AuthService.setExistingUserDetails($scope.userDetails);
            } else {
                console.log("** there was a problem updating user profile **");
            }
        });
    }
}]);