/**
 * Created by cgrieb on 2/25/16.
 */
angular.module('ContactsApp').controller('EditContactController',['$scope','$location', 'ContactService', function($scope, $location, ContactService) {
    $scope.contact = ContactService.returnContacts();

    $scope.navigateToList = function() {
        $location.path("/list");
    };

    $scope.saveExistingContact = function() {
        ContactService.saveContact($scope.contact).then(function(response){
            if (response) {
                console.warn("** contact with the ID of " + $scope.contact.id + " has been updated **");
            } else {
                console.warn("** contact with the ID of " + $scope.contact.id + " has not been updated **");

            }
            $scope.navigateToList();

        });
    };
}]);