/**
 * Created by cgrieb on 9/29/16.
 */
angular.module('ContactsApp').controller("LogoutController", ['$scope','$window','AuthService', function($scope,$window, AuthService) {
    AuthService.logout().then(function(response){
        AuthService.postLogoutOperation(response);
    });
}]);