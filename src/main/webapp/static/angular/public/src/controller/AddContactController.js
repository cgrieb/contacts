/**
 * Created by cgrieb on 1/24/16.
 */
angular.module('ContactsApp').controller('AddContactController', ["$scope","$location","ContactService","AuthService", function ($scope,$location, ContactService, AuthService) {
    $scope.timezones = {};
    $scope.selected = {};

    $scope.userDetails = AuthService.existingUserDetails();

    ContactService.getTimezone().then(function(response) {
        $scope.timezones = response;
    });

    $scope.submitNewContact = function() {
        var contact = {
            firstName:$scope.firstName,
            lastName:$scope.lastName,
            phoneNumber:$scope.phoneNumber,
            address:$scope.address,
            email:$scope.email,
            timeZone:$scope.selected.description,
            timezoneCode:$scope.selected.id,
            notes:$scope.notes,
            owner:$scope.userDetails.username
        };

        ContactService.addNewContact(contact).then(function(response){
           if(response) {
               console.info("Contact " + $scope.firstName + " " + $scope.lastName + " added");
               navigateToList();
           } else {
               $scope.resetFields();
           }
        });
    };

    $scope.resetFields = function() {
        $scope.firstName = "";
        $scope.lastName = "";
        $scope.phoneNumber = "";
        $scope.address = "";
        $scope.email = "";
        $scope.notes = "";
        $scope.selected = "";
    };

    $scope.booda =function() {
       return $scope.timezones[0].id;
    };


   var navigateToList = function() {
        $location.path("/list");
    };
}]);