/**
 * Created by cgrieb on 9/24/16.
 */
angular.module('ContactsApp').controller("LoginController", ['$scope','$window','$interval','AuthService', function($scope,$window,$interval, AuthService) {
    $scope.authenticated = false;
    $scope.password = "";
    $scope.username = "";

    $scope.logUserIn = function() {
        var authHeader = 'Basic ' + window.btoa($scope.username + ':' + $scope.password);
        AuthService.login(authHeader).then(function(response){
            $scope.authenticated = response;
            if($scope.authenticated) {
                $window.location = "/";
            } else {
                $scope.resetFields()
            }
        });
    };

    $scope.resetFields = function() {
        $scope.password = "";
        $scope.username = "";
    };
}]);