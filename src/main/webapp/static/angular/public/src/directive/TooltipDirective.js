/**
 * Created by cgrieb on 2/13/16.
 */
angular.module('ContactsApp').directive('toolTipDirective',[], function() {
   return {
       restrict: 'A',
       th: function(scope,element) {
           $(element).hover(function(){
               $(element).tooltip('show');
           },function(){
               $(element).tooltip('hide');
           });
       }
   }
});