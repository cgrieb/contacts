angular.module("ContactsApp", ["ngRoute", "ngResource", "ngMessages"])
    .config(function($routeProvider,$locationProvider){
        $routeProvider.when("/list", {
            controller: "GetContactsController",
            templateUrl: "/static/angular/public/view/list.html",
            requireAuth: true
        }).when("/", {
            templateUrl: "/static/angular/public/view/welcome.html",
            requireAuth: true
        }).when("/add", {
            controller: "AddContactController",
            templateUrl: "/static/angular/public/view/add.html",
            requireAuth: true
        }).when("/search", {
            controller: "SearchContactsController",
            templateUrl: "/static/angular/public/view/search.html",
            requireAuth: true
        }).when("/edit", {
            controller: "EditContactController",
            templateUrl: "/static/angular/public/view/edit.html",
            requireAuth: true
        }).when("/login", {
            controller: "LoginController",
            templateUrl: "/static/angular/public/view/login.html",
            requireLogin: false
        }).when("/create", {
            controller: "CreateController",
            templateUrl:"/static/angular/public/view/create.html",
            requireLogin: false
        }).when("/logout", {
            controller:"LogoutController",
            templateUrl:"/static/angular/public/view/logout.html"
        }).when("/profile", {
            controller:"ProfileController",
            templateUrl:"/static/angular/public/view/profile.html"
        }).otherwise( {
            redirectTo: "/"
        });
        $locationProvider.html5Mode(true);
});
