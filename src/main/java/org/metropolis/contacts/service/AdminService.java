package org.metropolis.contacts.service;

/**
 * Created by cgrieb on 10/18/16.
 */
public interface AdminService {
    /**
     * Clears system-level cache.
     */
    String clearCache();
}
