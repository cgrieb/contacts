package org.metropolis.contacts.service;

import org.metropolis.contacts.entity.User;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by cgrieb on 9/24/16.
 */
public interface AuthService {
    /**
     * Get currently logged in user details.
     * @return - User object, either of an actual user or a anonymous user.
     */
    User getUserDetails();

    /**
     * Service for extracting user authentication details.
     * @param userName - userName of the user we're checking.
     * @return - true/false.
     */
    boolean getAuthStatus(String userName);

    /**
     * Logs a user into the Contacts account.
     * @param httpServletRequest - HttpServletRequest.
     */
    boolean login(HttpServletRequest httpServletRequest);

    /**
     * Logout a user out of a current session.
     */
    boolean logout();

    /**
     * Adds a new User account object.
     * @param user - User object.
     */
    boolean addUser(User user);

    /**
     * Use SecurityContextHolder to obtain the currently logged in user.
     * @return - user who is logged in.
     */
    String getCurrentUsername();

    /**
     * Interface for updating a user profile, specifically login, first/last name.
     * @param user - User object.
     * @return updated/not updated.
     */
    void updateUserProfile(User user, UserDetails userDetails);
}
