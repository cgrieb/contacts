package org.metropolis.contacts.service;

import org.metropolis.contacts.entity.Contact;
import org.metropolis.contacts.entity.Fields;
import org.metropolis.contacts.entity.Text;
import org.metropolis.contacts.entity.Timezone;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * Created by cgrieb on 1/27/16.
 */
public interface ContactService {
    /**
     * Obtain a complete listing of all Contacts for a user.
     * @param userDetails - UserDetails object allows for extraction of user information.
     * @return
     */
    List<Contact> getAllContacts(UserDetails userDetails);

    /**
     * Search for specific contacts.
     * @param searchValue -> value we're searching on.
     * @return - List of Contacts.
     */
    List<Contact> findContacts(String searchValue);

    /**
     * Get all database stored text.
     * @return - List of texts.
     */
    List<Text> getText();

    /**
     * Adds a new contact for a secured user.
     * @param contact - Contact object we're adding.
     * @param userDetails - UserDetails object allows us to ensure that secured credentials match the Contact object owner.
     */
    void addNewContact(Contact contact, UserDetails userDetails);

    /**
     * Save an exist contact object for a secured user.
     * @param contact - Contact object we're adding.
     * @param userDetails - UserDetails object allows us to ensure that secured credentials match the Contact object owner..
     */
    void saveExistingContact(Contact contact, UserDetails userDetails);

    /**
     * Remove an existing contact.
     * @param contact - Contact we're removing.
     * @param userDetails - UserDetails object allows us to ensure that secured credentials match the Contact object owner.
     */
    void deleteExistingContact(Contact contact,  UserDetails userDetails);

    /**
     * Get all timezones located in the US.
     * @return - A List of timezones.
     */
    List<Timezone> getTimezones();
}
