package org.metropolis.contacts.service;

/**
 * Created by cgrieb on 11/27/16.
 */
public interface PasswordEncoder {
    /**
     * Encodes a String.
     * @param password - String we're encoding (e.g, password).
     * @return encoded password.
     */
    String encodeUserPassword(String password);
}
