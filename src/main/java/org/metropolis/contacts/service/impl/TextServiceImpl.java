package org.metropolis.contacts.service.impl;

import org.metropolis.contacts.entity.Text;
import org.metropolis.contacts.repository.TextRepository;
import org.metropolis.contacts.service.TextService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by cgrieb on 10/11/16.
 */
@Service
public class TextServiceImpl implements TextService {
    @Resource
    TextRepository textRepository;

    @Override
    public List<Text> getAllText() {
        return textRepository.findAll();
    }
}
