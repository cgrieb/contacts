package org.metropolis.contacts.service.impl;

import org.metropolis.contacts.entity.Role;
import org.metropolis.contacts.entity.User;
import org.metropolis.contacts.repository.RoleRepository;
import org.metropolis.contacts.repository.UserRepository;
import org.metropolis.contacts.service.AuthService;
import org.metropolis.contacts.service.PasswordEncoder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by cgrieb on 9/24/16.
 */
@Service
public class AuthServiceImpl implements AuthService {
    private static final String ANONYMOUS_USER = "anonymousUser";
    private static final Logger LOGGER =  Logger.getLogger(AuthService.class.getName());

    @Resource
    private UserRepository userRepository;

    @Resource
    private RoleRepository roleRepository;

    @Resource
    AuthenticationManager authenticationManager;

    @Resource
    PasswordEncoder passwordEncoder;

    @Override
    public User getUserDetails() {
        User user = new User();
        if(this.getCurrentUsername().equals(ANONYMOUS_USER)) {
            user.setUsername(ANONYMOUS_USER);
            user.setFirstName("Anonymous");
            user.setLastName("User");
            user.setRole("user");
            user.setPassword("");
            return user;
        }

        user = userRepository.getUserFromUsername(this.getCurrentUsername());
        user.setPassword("");
        return user;
    }

    @Override
    public boolean getAuthStatus(String userName) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName().equals(userName) && !authentication.getName().equals(ANONYMOUS_USER);
    }

    @Override
    public boolean login(HttpServletRequest httpServletRequest) {
        String[] credentials = this.getUsernameAndPassword(httpServletRequest);
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(credentials[0].toLowerCase(), credentials[1]);
        try {
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            if(authentication.isAuthenticated()) {
                return true;
            }
        } catch (final Throwable throwable) {
            LOGGER.warning("** Unable to authenticate " + credentials[0].toLowerCase() + " **");
        }
        return false;
    }

    @Override
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return SecurityContextHolder.getContext().getAuthentication() == null;
    }

    @Override
    public boolean addUser(User user) {
        if(this.exists(user.getUsername())) {
            return false;
        }

        user = this.validateNewUser(user);
        Role role = new Role();
        role.setUsername(user.getUsername());
        role.setRole("user");

        roleRepository.save(role);
        userRepository.save(user);

        return true;
    }

    @Override
    public  String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    private User validateNewUser(User user) {
        user.setUsername(user.getUsername().toLowerCase());

        // Ensure that the account is enabled.
        if(!user.isEnabled()) {
            user.setEnabled(true);
        }

        // Ensure that the account is not an admin level account.
        if(!user.getRole().equals("user")) {
            user.setRole("user");
        }

        // If the password < 5, generate a random UUID and strip out the dashes while
        if(user.getPassword().length()<5) {
            user.setPassword(UUID.randomUUID().toString().replace("-", ""));
            user.setEnabled(false);
        } else {
            user.setPassword(passwordEncoder.encodeUserPassword(user.getPassword()));
        }
        return user;
    }

    @Override
    public void updateUserProfile(User user,UserDetails userDetails) {
        User existing = userRepository.getUserFromUsername(user.getUsername());
        if(userDetails.getUsername().equals(user.getUsername())) {
            existing.setFirstName(user.getFirstName());
            existing.setLastName(user.getLastName());
            userRepository.save(existing);
        }
    }

    private String[] getUsernameAndPassword(HttpServletRequest httpServletRequest) {
        String encryptedCredentials = httpServletRequest.getHeader("Authentication").substring(("Basic").length()).trim();
        String[] credentials = new String(Base64.getDecoder().decode(encryptedCredentials), Charset.forName("UTF-8")).split(":",2);
        return credentials;
    }

    private boolean exists(String username) {
        return (userRepository.exists(username));
    }
}
