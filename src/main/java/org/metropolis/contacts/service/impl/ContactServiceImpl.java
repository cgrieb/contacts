package org.metropolis.contacts.service.impl;

import org.metropolis.contacts.repository.ContactRepository;
import org.metropolis.contacts.repository.FieldsRepository;
import org.metropolis.contacts.repository.TextRepository;
import org.metropolis.contacts.entity.Contact;
import org.metropolis.contacts.entity.Fields;
import org.metropolis.contacts.entity.Text;
import org.metropolis.contacts.entity.Timezone;
import org.metropolis.contacts.repository.TimezoneRepository;
import org.metropolis.contacts.service.ContactService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by cgrieb on 1/31/16.
 */
@Service
public class ContactServiceImpl implements ContactService {
    @Resource
    TextRepository textRepository;

    @Resource
    ContactRepository contactRepository;

    @Resource
    FieldsRepository fieldsRepository;

    @Resource
    TimezoneRepository timezoneRepository;

    @Override
    public List<Contact> getAllContacts(UserDetails userDetails) {
        return contactRepository.getAllContacts(userDetails.getUsername());
    }

    @Override
    public List<Contact> findContacts(String searchValue) {
        return contactRepository.findContactFromString(searchValue);
    }

    @Override
    public List<Text> getText() {
        return textRepository.findAll();
    }

    @Override
    public List<Timezone> getTimezones() {
        return timezoneRepository.findAll();
    }

    @Override
    @CacheEvict(value = "contactCache", allEntries = true, key = "#contact.owner")
    public void addNewContact(Contact contact, UserDetails userDetails) {
        if(userDetails.getUsername().equals(contact.getOwner())) {
            contactRepository.save(contact);
        }
    }

    @Override
    @CacheEvict(value = "contactCache", allEntries = true, key = "#contact.owner")
    public void saveExistingContact(Contact contact,UserDetails userDetails) {
        if(userDetails.getUsername().equals(contact.getOwner())) {
            contactRepository.save(contact);
        }
    }

    @Override
    @CacheEvict(value = "contactCache", allEntries = true, key = "#contact.owner")
    public void deleteExistingContact(Contact contact, UserDetails userDetails) {
        if(userDetails.getUsername().equals(contact.getOwner())) {
            contactRepository.delete(contact);
        }
    }
}
