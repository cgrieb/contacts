package org.metropolis.contacts.service.impl;

import org.metropolis.contacts.service.AdminService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by cgrieb on 10/18/16.
 */
@Service
public class AdminServiceImpl implements AdminService {
    private static final String TEXT_CACHE = "textCache";
    private static final String CONTACT_CACHE = "contactCache";
    private static final String TIMEZONE_CACHE = "timezoneCache";

    @Override
    @CacheEvict(value = {TEXT_CACHE,CONTACT_CACHE,TIMEZONE_CACHE}, allEntries = true)
    public String clearCache() {
        return "** Cached has been cleared on " + new Date().toString() + " **";
    }
}
