package org.metropolis.contacts.service;

import org.metropolis.contacts.entity.Text;

import java.util.List;

/**
 * Created by cgrieb on 10/11/16.
 */
public interface TextService {
    /**
     * Obtain a listing of all available text.
     * @return
     */
    List<Text> getAllText();
}
