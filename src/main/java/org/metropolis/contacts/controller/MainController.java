package org.metropolis.contacts.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by cgrieb on 9/24/16.
 */
@Controller
public class MainController {
    /**
     * Return the view to our main index Angular page.
     * @return - "main"
     */
    @RequestMapping(value = "/*", method = RequestMethod.GET)
    public String getMain() {
        return "main";
    }
}
