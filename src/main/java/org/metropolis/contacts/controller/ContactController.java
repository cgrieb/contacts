package org.metropolis.contacts.controller;

import org.metropolis.contacts.entity.Contact;
import org.metropolis.contacts.entity.Fields;
import org.metropolis.contacts.entity.Timezone;
import org.metropolis.contacts.service.ContactService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/contacts")
@PreAuthorize("hasAnyRole('admin','user')")
public class ContactController {
    @Resource
    private ContactService contactService;

    /**
     * Add a new contact.
     * @param contact - Contact object to add.
     */
    @RequestMapping(value = "/addNewContact", method = RequestMethod.POST)
    public @ResponseBody void addNewContact(@RequestBody Contact contact, @AuthenticationPrincipal UserDetails userDetails) {
        contactService.addNewContact(contact, userDetails);
    }

    /**
     * Save an existing contact.
     * @param contact - Contact object that already exists to save.
     */
    @RequestMapping(value = "/saveContact", method = RequestMethod.POST)
    public @ResponseBody void saveContact(@RequestBody Contact contact, @AuthenticationPrincipal UserDetails userDetails) {
        contactService.saveExistingContact(contact,userDetails);
    }

    /**
     * Delete an existing contact from the UI.
     * @return number of contacts deleted (this should always be 1).
     */
    @RequestMapping(value = "/deleteContact", method = RequestMethod.POST)
    public @ResponseBody void deleteContact(@RequestBody Contact contact, @AuthenticationPrincipal UserDetails userDetails) {
         contactService.deleteExistingContact(contact, userDetails);
    }

    /**
     * Get a complete listing of all contacts.
     * @return A List of contacts.
     */
	@RequestMapping(value = "/getContacts", method = RequestMethod.GET)
	public @ResponseBody List<Contact> getContacts(@AuthenticationPrincipal UserDetails userDetails) {
        return this.contactService.getAllContacts(userDetails);
    }

    /**
     * Path variable searching.
     * @param searchValue -> value we're searching for.
     * @return A List of contacts.
     */
    @RequestMapping(value = "/getContacts/{searchValue}", method = RequestMethod.GET)
    public @ResponseBody List<Contact> getContactFromSearch(@PathVariable String searchValue) {
        return this.contactService.findContacts(searchValue);
    }

    /**
     * Request param searching.
     * @param searchValue -> value we're searching for.
     * @return A List of contacts.
     */
    @RequestMapping(value = "/searchContacts", method = RequestMethod.GET)
    public @ResponseBody List<Contact> searchContacts(@RequestParam(value = "searchValue", required = true) String searchValue) {
        return this.contactService.findContacts(searchValue);
    }

    /**
     * Timezone entry point.
     * @return A List of timezones.
     */
    @RequestMapping(value = "/getTimezones", method = RequestMethod.GET)
    public @ResponseBody List<Timezone> getTimezones() {
        return  contactService.getTimezones();
    }
}