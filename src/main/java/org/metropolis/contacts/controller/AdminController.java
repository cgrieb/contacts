package org.metropolis.contacts.controller;

import org.metropolis.contacts.service.AdminService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by cgrieb on 9/29/16.
 */
@Controller
@RequestMapping(value = "/admin")
@PreAuthorize("hasAnyRole('admin')")
public class AdminController {
    @Resource
    private AdminService adminService;

    @RequestMapping(value = "/clearCache", method = RequestMethod.GET)
    public @ResponseBody String clearCache() {
        return adminService.clearCache();
    }
}
