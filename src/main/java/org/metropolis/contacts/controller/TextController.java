package org.metropolis.contacts.controller;

import org.metropolis.contacts.entity.Text;
import org.metropolis.contacts.service.TextService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by cgrieb on 10/1/16.
 */
@RequestMapping(value = "/text")
@Controller
public class TextController {
    @Resource
    private TextService textService;

    /**
     * Text service entry point - this is a complete listing of all
     * database stored text.
     * @return - List of text.
     */
    @RequestMapping(value="/getText", method = RequestMethod.GET)
    public @ResponseBody
    List<Text> getText() {
        return textService.getAllText();
    }
}
