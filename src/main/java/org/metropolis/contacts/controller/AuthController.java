package org.metropolis.contacts.controller;

import org.metropolis.contacts.entity.User;
import org.metropolis.contacts.service.AuthService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by cgrieb on 9/24/16.
 */
@Controller
@RequestMapping(value = "/auth")
public class AuthController {
    @Resource
    private AuthService authService;

    /**
     * Get a User object related to our users.
     * @return - User object.
     */
    @RequestMapping(value = "/getUserDetails", method = RequestMethod.GET)
    public @ResponseBody User getUserDetails() {
        return authService.getUserDetails();
    }

    /**
     * Determine if a user it logged in or not.
     * @param userName - userName.
     * @return - User is logged in/user is not logged in, true/false.
     */
    @RequestMapping(value = "/getAuthStatus", method = RequestMethod.POST)
    public @ResponseBody boolean getAuthStatus(@RequestParam(value = "userName", required = true) String userName) {
        return authService.getAuthStatus(userName);
    }

    /**
     * Entry point for logging a user into the system.
     * @param httpServletRequest - HttpServletRequest.
     * @return true/false, user successfully logged in/not successfully logged in.
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public @ResponseBody boolean login(HttpServletRequest httpServletRequest) {
        return authService.login(httpServletRequest);
    }

    /**
     * Entry point for logging out a user.
     * @return - User was successfully logged out, not successfully logged out.
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public @ResponseBody boolean logout() {
        return authService.logout();
    }

    /**
     * Add a new userAccount to the database.
     * @param user - A User object containing our user.
     */
    @RequestMapping(value = "/createUserAccount", method = RequestMethod.POST)
    public @ResponseBody boolean createUserAccount(@RequestBody User user) {
        return authService.addUser(user);
    }

    /**
     * Update an existing user account.
     * @param user - User object.
     * @return updated successfully/not updated successfully.
     */
    @PreAuthorize("hasAnyRole('admin','user')")
    @RequestMapping(value = "/updateUserDetails", method = RequestMethod.POST)
    public void updateUserProfile(@RequestBody User user, @AuthenticationPrincipal UserDetails userDetails) {
        authService.updateUserProfile(user,userDetails);
    }
}
