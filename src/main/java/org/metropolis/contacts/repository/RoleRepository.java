package org.metropolis.contacts.repository;

import org.metropolis.contacts.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by cgrieb on 10/3/16.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role,String> {
}
