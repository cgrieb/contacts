package org.metropolis.contacts.repository;

import org.metropolis.contacts.entity.Contact;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 9/23/16.
 */
@Repository
public interface ContactRepository extends JpaRepository<Contact,Integer> {
    @Query("select c from Contact c where c.owner = :owner")
    @Cacheable(value = "contactCache", key="#a0")
    List<Contact> getAllContacts(@Param(value = "owner") String owner);

    @Query("select c from Contact  c where c.firstName like :string or c.lastName like :string or c.address like :string or c.phoneNumber " +
            "like :string or c.timeZone like :string or c.timezoneCode like :string or c.email like :string")
    List<Contact> findContactFromString(@Param(value = "string") String string);


}
