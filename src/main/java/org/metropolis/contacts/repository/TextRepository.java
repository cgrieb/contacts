package org.metropolis.contacts.repository;

import org.metropolis.contacts.entity.Text;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 2/14/16.
 */
@Repository
public interface TextRepository extends JpaRepository<Text,String> {
    @Cacheable(value = "textCache")
    @Override
    @Query("select t from Text t")
    List<Text> findAll();
}
