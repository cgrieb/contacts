package org.metropolis.contacts.repository;

import org.metropolis.contacts.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by cgrieb on 9/24/16.
 */
@Repository
public interface UserRepository extends JpaRepository<User,String> {
    @Query("select u from User u where u.username = :username")
    User getUserFromUsername(@Param(value = "username") String username);
}
