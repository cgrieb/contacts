package org.metropolis.contacts.repository;

import org.metropolis.contacts.entity.Fields;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 9/23/16.
 */
@Repository
public interface FieldsRepository extends JpaRepository<Fields,String> {
    @Query("select f from Fields f")
    List<Fields> getAllFieldsIfExists();
}
