package org.metropolis.contacts.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by cgrieb on 2/20/16.
 */
@Entity
@Table(name = "timezone")
public class Timezone {
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "description")
    private String description;
    @Column(name = "utc_code")
    private String utcCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUtcCode() {
        return utcCode;
    }

    public void setUtcCode(String utcCode) {
        this.utcCode = utcCode;
    }
}
