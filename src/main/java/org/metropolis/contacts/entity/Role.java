package org.metropolis.contacts.entity;

import javax.persistence.*;

/**
 * Created by cgrieb on 9/24/16.
 */
@Entity
@Table(name = "contact_role")
public class Role {
    @Id
    @Column(name = "username")
    private String username;

    @Column(name = "role")
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
