package org.metropolis.contacts.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by cgrieb on 2/14/16.
 */
@Entity
@Table(name = "text")
public class Text {
    @Id
    @Column(name = "id")
    String id;
    @Column(name = "value")
    String value;
    public String getId() {
        return id;
    }

    public void setId(String tag) {
        this.id = tag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
