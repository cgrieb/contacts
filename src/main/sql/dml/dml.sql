insert into contact_roles (role,description) values('admin', 'Administrator');
insert into contact_roles (role,description) values('user', 'User');

insert into timezone (id,description,utc_code) values('ASK', 'Alaska Standard Time', 'GTM-09:00');
insert into timezone (id,description,utc_code) values('PST', 'Pacific Standard Time', 'GTM-08:00');
insert into timezone (id,description,utc_code) values('MST', 'Mountain Standard Time', 'GTM-07:00');
insert into timezone (id,description,utc_code) values('CST', 'Central Standard Time', 'GTM-06:00');
insert into timezone (id,description,utc_code) values('EST', 'Eastern Standard Time', 'GTM-05:00');
insert into timezone (id,description,utc_code) values('AST', 'Atlantic Standard Time', 'GTM-04:00');

insert into text (id,value) values('contacts.main.list.link', 'Get All Contacts');
insert into text (id,value) values('contacts.main.add.link', 'Add New Contact');
insert into text (id,value) values('contacts.main.main.link', 'Return to Main');
insert into text (id,value) values('contacts.main.nav.tab', 'Navigation');

insert into text (id,value) values('contacts.welcome.body.text', 'Use the above menu to add, remove and search for contacts');
insert into text (id,value) values('contacts.welcome.head.text', 'Metropolis Contacts');

insert into text (id,value) values('contacts.add.new.contact', 'Add a new Contact');
insert into text (id,value) values('contacts.add.save.new', 'Save new Contact!');
insert into text (id,value) values('contacts.add.address.placeholder', 'Address');
insert into text (id,value) values('contacts.add.firstname.placeholder', 'First Name');
insert into text (id,value) values('contacts.add.lastname.placeholder', 'Last Name');
insert into text (id,value) values('contacts.add.phone.placeholder', 'Phone Number');
insert into text (id,value) values('contacts.add.time.select', 'Select a timezone');
insert into text (id,value) values('contacts.add.save.cancel', 'Cancel...');
insert into text (id,value) values('contacts.add.email.placeholder', 'Email');
insert into text (id,value) values('contacts.add.notes.placeholder', 'Notes');
insert into text (id,value) values('contacts.add.timezone.required', 'Timezone *');
insert into text (id,value) values('contacts.add.firstname.required', 'First Name *');
insert into text (id,value) values('contacts.add.lastname.required', 'Last Name *');
insert into text (id,value) values('contacts.add.address.required', 'Address *');
insert into text (id,value) values('contacts.add.phonenumber.required', 'Phonenumber *');
insert into text (id,value) values('contacts.add.email.required', 'Email *');
insert into text (id,value) values('contacts.add.notes.required', 'Notes');
insert into text (id,value) values('contacts.add.required.required', '* Required');

insert into text (id,value) values('contacts.search.button.go', 'Go!');
insert into text (id,value) values('contacts.search.input.placeholder', 'Search for contacts..');
insert into text (id,value) values('contacts.search.results.notfound', 'Sorry, no contacts were found for that search query...');
insert into text (id,value) values('contacts.search.firstname.paragraph', 'First Name');
insert into text (id,value) values('contacts.search.lastname.paragraph', 'Last Name');
insert into text (id,value) values('contacts.search.phone.paragraph', 'Phone');
insert into text (id,value) values('contacts.search.address.paragraph', 'Address');
insert into text (id,value) values('contacts.search.email.paragraph', 'Email');
insert into text (id,value) values('contacts.search.timezone.paragraph', 'Timezone');
insert into text (id,value) values('contacts.search.search.header', 'Search for contacts');

insert into text (id,value) values('contacts.list.edit.notification','Edit Contact');
insert into text (id,value) values('contacts.list.delete.notification','Delete Contact');
insert into text (id,value) values('contacts.list.notes.paragraph','Notes');
insert into text (id,value) values('contacts.list.searchresults.paragraph','No search results were found for');
insert into text (id,value) values('contacts.list.searchresults.note','Please try again');
insert into text (id,value) values('contacts.list.noresults.paragraph','Your contact list is empty - please on the ''add'' link to add a contact');
insert into text (id,value) values('contacts.list.search.reset','Reset Search');
insert into text (id,value) values('contacts.welcome.body.info', 'This is a basic contact application utilizing Spring MVC, AngularJS, and MySql. Exciting and fun!');

insert into text (id,value) values('contacts.edit.exiting.title','Edit existing contact');
insert into text (id,value) values('contacts.edit.exiting.required','required');
insert into text (id,value) values('contacts.edit.exiting.firstName','First Name');
insert into text (id,value) values('contacts.edit.exiting.lastName','Last Name');
insert into text (id,value) values('contacts.edit.exiting.phoneNumber','Phone');
insert into text (id,value) values('contacts.edit.exiting.address','Address');
insert into text (id,value) values('contacts.edit.exiting.email','Email');
insert into text (id,value) values('contacts.edit.exiting.notes','Notes');
insert into text (id,value) values('contacts.edit.exiting.requiredFields','** First name, last name, address, email, and phone number are required fields **');

insert into text (id,value) values('contacts.user.login.message','Login to view contacts');
insert into text (id,value) values('contacts.user.login.login','Click to login');
insert into text (id,value) values('contacts.login.create.account','Click here to create an account');
insert into text (id,value) values('contacts.login.create.title','Create an account');
insert into text (id,value) values('contacts.login.create.login','Email - this is your login');
insert into text (id,value) values('contacts.login.create.return','Return to the login page');
insert into text (id,value) values('contacts.login.create.create','Create Account');
insert into text (id,value) values('contacts.main.profile.link', 'Profile');
insert into text (id,value) values('contacts.profile.panel.heading', 'Update your profile');
insert into text (id,value) values('contacts.profile.update.firstname', 'First Name');
insert into text (id,value) values('contacts.profile.update.profile', 'Update Profile');
insert into text (id,value) values('contacts.profile.update.cancel', 'Cancel');
insert into text (id,value) values('contacts.login.create.resetvalues', 'Reset Values');
insert into text (id,value) values('contacts.profile.update.lastname', 'Last Name');