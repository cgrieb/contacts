create table contacts (
  id int (11) not null AUTO_INCREMENT,
  primary key (id),
  first_name varchar (80) not null,
  last_name varchar (80) not null,
  phone_number varchar (80) not null,
  address varchar (80) not null,
  timezone varchar (80) not null,
  timezone_code varchar(4) not null,
  email varchar(80) not null,
  notes varchar(225),
  owner varchar(45)
);

create table fields (
  id int (11) not null AUTO_INCREMENT,
  PRIMARY KEY (id),
  first_name varchar (25) not null,
  last_name varchar (25) not null,
  phone_number varchar (25) not null,
  address varchar (25) not null,
  timezone varchar (25) not null,
  email varchar(25) not null
);

create table text(
  id varchar(100) not null,
  primary key (id),
  value varchar(4000) not null
);

create table timezone (
  id varchar(4) not null,
  primary key (id),
  description varchar(200) not null,
  utc_code varchar(12) not null
);

create table contact_user(
  username varchar(45) not null primary key,
  password varchar(1024) not null,
  first_name varchar(45) not null,
  last_name varchar(45) not null,
  enabled bit not null,
  role varchar(45) not null
);

create table contact_role(
  username varchar(45) not null primary key,
  role varchar(11) not null
);